<?php declare(strict_types=1);

namespace App\Skill;

use App\Creature\CreatureInterface;
use ReflectionClass;

/**
 * Base class offering some helpers.
 */
abstract class Skill implements SkillInterface
{
    private ?CreatureInterface $owner;

    public static function getName(): string
    {
        $reflect = new ReflectionClass(get_called_class());
        return str_replace('Skill', '', $reflect->getShortName());
    }

    public function __construct(CreatureInterface $owner = null)
    {
        $this->owner = $owner;
    }


    public function getOwner(): ?CreatureInterface
    {
        return $this->owner;
    }

    public function setOwner(?CreatureInterface $owner): void
    {
        $this->owner = $owner;
    }

    public function __toString(): string
    {
        return self::getName();
    }

}