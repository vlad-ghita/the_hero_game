<?php declare(strict_types=1);

namespace App\Skill;

/**
 * Defender has a chance to take only half damage.
 */
class MagicShieldSkill extends PercentSkill
{
}