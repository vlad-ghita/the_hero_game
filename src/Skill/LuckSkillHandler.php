<?php declare(strict_types=1);

namespace App\Skill;

use App\Battle\BattleRoundInterface;

class LuckSkillHandler extends SkillHandler
{

    public static function watchedSkills(): array
    {
        return [LuckSkill::getName()];
    }

    public function apply(SkillInterface $skill, BattleRoundInterface $round): bool
    {
        if ($skill->getOwner() !== $round->getDefender()) {
            return false;
        }

        $skill->setChance($round->getDefender()->getLuck());

        if (!$this->percentSkillIsActive($skill)) {
            return false;
        }

        $round->setDamage(0);

        return true;
    }

}