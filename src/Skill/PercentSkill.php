<?php declare(strict_types=1);

namespace App\Skill;

use App\Creature\CreatureInterface;

/**
 * Support for percentage based skills.
 */
abstract class PercentSkill extends Skill implements PercentSkillInterface
{

    public function __construct(
        protected int $chance,
        CreatureInterface $owner = null,
    )
    {
        parent::__construct($owner);
    }

    public function getChance(): int
    {
        return $this->chance;
    }

    public function setChance(int $chance): void
    {
        $this->chance = $chance;
    }

}