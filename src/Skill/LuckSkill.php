<?php declare(strict_types=1);

namespace App\Skill;

use App\Creature\CreatureInterface;

/**
 * When defender is lucky, damage is avoided.
 */
class LuckSkill extends PercentSkill
{

    public function __construct(
        int               $chance = 0,
        CreatureInterface $owner = null,
    )
    {
        parent::__construct($chance, $owner);
    }

}