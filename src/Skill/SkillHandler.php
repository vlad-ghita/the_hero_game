<?php declare(strict_types=1);

namespace App\Skill;

use App\Probability\ProbabilityInterface;

abstract class SkillHandler implements SkillHandlerInterface
{
    public function __construct(
        protected ProbabilityInterface $probability,
    )
    {
    }

    protected function percentSkillIsActive(SkillInterface $skill): bool
    {
        if (!$skill instanceof PercentSkillInterface) {
            return true;
        }

        return $this->probability->fromValues($skill::MIN, $skill::MAX, $skill->getChance());
    }

}