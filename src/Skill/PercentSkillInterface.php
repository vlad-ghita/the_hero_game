<?php declare(strict_types=1);

namespace App\Skill;

/**
 * Support for percentage based skills.
 */
interface PercentSkillInterface
{
    const MIN = 0;
    const MAX = 100;

    public function getChance(): int;
}