<?php declare(strict_types=1);

namespace App\Skill;

use App\Battle\BattleRoundInterface;

/**
 * Encapsulates logic to handle a skill.
 */
interface SkillHandlerInterface
{

    /**
     * Returns skills for which this handler should be called.
     * Each element must be skill name as returned by @see SkillInterface::getName()
     *
     * @return string[]
     */
    public static function watchedSkills(): array;

    /**
     * Applies skill to round.
     *
     * Returns true if skill was applied, false otherwise.
     *
     * @param SkillInterface $skill
     * @param BattleRoundInterface $round
     * @return bool
     */
    public function apply(SkillInterface $skill, BattleRoundInterface $round): bool;

}