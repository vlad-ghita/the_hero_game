<?php declare(strict_types=1);

namespace App\Skill;

use App\Battle\BattleRoundInterface;

class MagicShieldSkillHandler extends SkillHandler
{

    public static function watchedSkills(): array
    {
        return [MagicShieldSkill::getName()];
    }

    public function apply(SkillInterface $skill, BattleRoundInterface $round): bool
    {
        if (
            $skill->getOwner() !== $round->getDefender()
            || !$this->percentSkillIsActive($skill)
        ) {
            return false;
        }

        $round->setDamage(intval(ceil($round->getDamage() / 2)));

        return true;
    }

}