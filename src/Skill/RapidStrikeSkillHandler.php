<?php declare(strict_types=1);

namespace App\Skill;

use App\Battle\BattleRoundInterface;

class RapidStrikeSkillHandler extends SkillHandler
{

    public static function watchedSkills(): array
    {
        return [RapidStrikeSkill::getName()];
    }

    public function apply(SkillInterface $skill, BattleRoundInterface $round): bool
    {
        if (
            $skill->getOwner() !== $round->getAttacker()
            || !$this->percentSkillIsActive($skill)
        ) {
            return false;
        }

        $round->setNextAttacker($round->getAttacker());
        $round->setNextDefender($round->getDefender());

        return true;
    }

}