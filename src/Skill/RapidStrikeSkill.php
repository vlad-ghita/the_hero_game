<?php declare(strict_types=1);

namespace App\Skill;

/**
 * Attacker has a chance to attack twice in a row.
 */
class RapidStrikeSkill extends PercentSkill
{
}