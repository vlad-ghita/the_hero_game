<?php declare(strict_types=1);

namespace App\Skill;

class SkillHandlerCollection
{
    private array $handlersPerSkill = [];

    /**
     * @param SkillHandlerInterface[] $skillHandlers
     */
    public function __construct(
        private iterable $skillHandlers
    )
    {
        foreach ($this->skillHandlers as $skillHandler) {
            foreach ($skillHandler::watchedSkills() as $skillName) {
                $this->handlersPerSkill[$skillName][] = $skillHandler;
            }
        }
    }

    /**
     * Given a skill, returns handlers knowing to handle it.
     *
     * @param SkillInterface $skill
     * @return SkillHandlerInterface[]
     */
    public function forSkill(SkillInterface $skill): array
    {
        return $this->handlersPerSkill[$skill::getName()] ?? [];
    }

}