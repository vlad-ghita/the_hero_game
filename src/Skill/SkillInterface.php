<?php declare(strict_types=1);

namespace App\Skill;

use App\Creature\CreatureInterface;

interface SkillInterface
{
    /**
     * Unique name to identify this skill.
     */
    public static function getName(): string;

    /**
     * Creature owning the skill.
     *
     * @return ?CreatureInterface
     */
    public function getOwner(): ?CreatureInterface;

    public function setOwner(?CreatureInterface $owner): void;

}