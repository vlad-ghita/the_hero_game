<?php declare(strict_types=1);

namespace App\Pattern;

/**
 * Builds object directly.
 */
class ObjectBuilder implements BuilderDecoratorInterface
{
    use DecoratorTrait;

    public function __construct(
        private string $class,
    )
    {
        $this->reset();
    }

    public function build(): object
    {
        $object = $this->getDecorated();
        $this->reset();
        return $object;
    }

    private function reset(): void
    {
        $this->setDecorated(new $this->class());
    }

}