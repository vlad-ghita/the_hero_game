<?php declare(strict_types=1);

namespace App\Pattern;

use App\Exception\ValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Validates built object.
 */
class ValidationBuilder extends BuilderDecorator
{

    public function __construct(
        BuilderInterface             $decorated,
        protected ValidatorInterface $validator
    )
    {
        parent::__construct($decorated);
    }

    public function build(): object
    {
        $object = parent::build();

        if (0 !== ($errors = $this->validator->validate($object))->count()) {
            throw new ValidationException($errors);
        }

        return $object;
    }

}