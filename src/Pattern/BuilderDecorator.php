<?php declare(strict_types=1);

namespace App\Pattern;

abstract class BuilderDecorator implements BuilderDecoratorInterface
{
    use BuilderDecoratorTrait;

    public function __construct(BuilderInterface $decorated)
    {
        $this->decorated = $decorated;
    }

}