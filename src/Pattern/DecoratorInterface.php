<?php declare(strict_types=1);

namespace App\Pattern;

/**
 * Knows to decorate an object.
 */
interface DecoratorInterface
{

    public function getDecorated(): object;

    public function setDecorated(object $decorated): void;

}