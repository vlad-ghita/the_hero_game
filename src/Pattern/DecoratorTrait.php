<?php declare(strict_types=1);

namespace App\Pattern;

trait DecoratorTrait
{

    protected object $decorated;

    public function getDecorated(): object
    {
        return $this->decorated;
    }

    public function setDecorated(object $decorated): void
    {
        $this->decorated = $decorated;
    }

    public function __call(string $name, array $arguments)
    {
        call_user_func_array(array($this->decorated, $name), $arguments);
    }

}