<?php declare(strict_types=1);

namespace App\Pattern;

/**
 * Decorates object builder.
 */
interface BuilderDecoratorInterface extends BuilderInterface, DecoratorInterface
{
}