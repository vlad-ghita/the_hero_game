<?php declare(strict_types=1);

namespace App\Pattern;

trait BuilderDecoratorTrait
{
    use DecoratorTrait;

    public function build(): object
    {
        return $this->getDecorated()->build();
    }
}