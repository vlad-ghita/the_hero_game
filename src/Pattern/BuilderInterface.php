<?php declare(strict_types=1);

namespace App\Pattern;

/**
 * Knows to build an object.
 */
interface BuilderInterface
{

    /**
     * Builds the object.
     *
     * @return object
     */
    public function build(): object;

}