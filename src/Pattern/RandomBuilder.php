<?php declare(strict_types=1);

namespace App\Pattern;

use App\Randomizer\RandomizerInterface;

/**
 * Builds object with random values.
 */
class RandomBuilder extends BuilderDecorator
{
    public function __construct(
        BuilderInterface            $decorated,
        private RandomizerInterface $randomizer,
    )
    {
        parent::__construct($decorated);
    }


    public function build(): object
    {
        $this->randomizer->randomize($this->getInnerObject());

        return parent::build();
    }

    /**
     * Returns most inner decorated object.
     */
    private function getInnerObject(): object
    {
        $result = $this;

        while ($result instanceof DecoratorInterface) {
            $result = $result->getDecorated();
        }

        return $result;
    }
}