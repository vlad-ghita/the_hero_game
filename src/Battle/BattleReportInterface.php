<?php declare(strict_types=1);

namespace App\Battle;

use App\Creature\CreatureInterface;

/**
 * Holds information about a battle.
 */
interface BattleReportInterface
{
    /**
     * @return BattleRoundInterface[]
     */
    public function getRounds(): array;

    public function addRound(BattleRoundInterface $round): void;

    public function winnerExists(): bool;

    public function getWinner(): CreatureInterface;

}