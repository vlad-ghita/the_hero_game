<?php declare(strict_types=1);

namespace App\Battle;

use App\Creature\CreatureInterface;
use App\Skill\SkillInterface;

class BattleRound implements BattleRoundInterface
{
    private ?int $damage;
    private ?int $defenderOldHealth;
    private ?int $defenderNewHealth;
    private ?CreatureInterface $nextAttacker = null;
    private ?CreatureInterface $nextDefender = null;
    /** @var SkillInterface[] */
    private array $attackerSkillsUsed = [];
    /** @var SkillInterface[] */
    private array $defenderSkillsUsed = [];

    public function __construct(
        private int               $idx,
        private CreatureInterface $attacker,
        private CreatureInterface $defender,
    )
    {
        $this->damage = $this->attacker->getStrength() - $this->defender->getDefence();
        $this->defenderOldHealth = $this->defender->getHealth();
        $this->defenderNewHealth = $this->defenderOldHealth;
    }

    public function getIdx(): int
    {
        return $this->idx;
    }

    public function getAttacker(): CreatureInterface
    {
        return $this->attacker;
    }

    public function getDefender(): CreatureInterface
    {
        return $this->defender;
    }

    public function getNextAttacker(): ?CreatureInterface
    {
        return $this->nextAttacker;
    }

    public function setNextAttacker(?CreatureInterface $nextAttacker): void
    {
        $this->nextAttacker = $nextAttacker;
    }

    public function getNextDefender(): ?CreatureInterface
    {
        return $this->nextDefender;
    }

    public function setNextDefender(?CreatureInterface $nextDefender): void
    {
        $this->nextDefender = $nextDefender;
    }

    public function getDamage(): int
    {
        return $this->damage;
    }

    public function setDamage(int $damage): void
    {
        $this->damage = $damage;
    }

    public function getDefenderOldHealth(): int
    {
        return $this->defenderOldHealth;
    }

    public function getDefenderNewHealth(): int
    {
        return $this->defenderNewHealth;
    }

    /** @return SkillInterface[] */
    public function getAttackerSkillsUsed(): array
    {
        return $this->attackerSkillsUsed;
    }

    public function addAttackerSkillUsed(SkillInterface $skill): void
    {
        $this->attackerSkillsUsed[] = $skill;
    }

    /** @return SkillInterface[] */
    public function getDefenderSkillsUsed(): array
    {
        return $this->defenderSkillsUsed;
    }

    public function addDefenderSkillUsed(SkillInterface $skill): void
    {
        $this->defenderSkillsUsed[] = $skill;
    }

    public function doDamage(): void
    {
        $this->defender->takeDamage($this->getDamage());
        $this->defenderNewHealth = $this->defender->getHealth();
    }

}