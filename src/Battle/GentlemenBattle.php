<?php declare(strict_types=1);

namespace App\Battle;

use App\Creature\CreatureInterface;
use App\Skill\SkillHandlerCollection;
use Closure;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Fighters are gentlemen.
 *
 * They attack alternatively, unless skills get in the way.
 */
class GentlemenBattle implements BattleInterface
{

    public function __construct(
        private ParameterBagInterface  $params,
        private SkillHandlerCollection $skillHandlers
    )
    {
    }

    public function fight(CreatureInterface $creature1, CreatureInterface $creature2): BattleReportInterface
    {
        // determine first attacker
        if ($creature2->getSpeed() > $creature1->getSpeed() || $creature2->getLuck() > $creature1->getLuck()) {
            $attacker = $creature2;
            $defender = $creature1;
        } else {
            $attacker = $creature1;
            $defender = $creature2;
        }

        // fight!
        $report = new BattleReport($creature1, $creature2);
        $round = new BattleRound(-1, $defender, $attacker);

        while ($round->getIdx() < $this->params->get('app.battle.max_rounds') - 1 && $attacker->getHealth() > 0 && $defender->getHealth() > 0) {
            $round = new BattleRound(
                $round->getIdx() + 1,
                $round->getNextAttacker() ?? $round->getDefender(),
                $round->getNextDefender() ?? $round->getAttacker(),
            );

            $report->addRound($round);

            $this->handleSkills($round, $round->getAttacker(), fn($skill) => $round->addAttackerSkillUsed($skill));
            $this->handleSkills($round, $round->getDefender(), fn($skill) => $round->addDefenderSkillUsed($skill));

            // if damage is negative, we don't want to increase health ...
            if ($round->getDamage() >= 0) {
                $round->doDamage();
            }
        }

        return $report;
    }

    /**
     * Handles skills for a creature in a round.
     *
     * @param BattleRoundInterface $round
     * @param CreatureInterface $creature
     * @param Closure $addUsedSkillCallback
     */
    private function handleSkills(BattleRoundInterface $round, CreatureInterface $creature, Closure $addUsedSkillCallback): void
    {
        foreach ($creature->getSkills() as $skill) {
            foreach ($this->skillHandlers->forSkill($skill) as $skillHandler) {
                if ($skillHandler->apply($skill, $round)) {
                    $addUsedSkillCallback($skill);
                }
            }
        }
    }

}