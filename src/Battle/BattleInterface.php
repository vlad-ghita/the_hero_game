<?php declare(strict_types=1);

namespace App\Battle;

use App\Creature\CreatureInterface;

/**
 * Runs a fight between two creatures.
 */
interface BattleInterface
{

    public function fight(CreatureInterface $creature1, CreatureInterface $creature2): BattleReportInterface;

}