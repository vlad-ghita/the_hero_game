<?php declare(strict_types=1);

namespace App\Battle;

use App\Creature\CreatureInterface;
use App\Skill\SkillInterface;

/**
 * Holds information about a battle round.
 */
interface BattleRoundInterface
{
    public function getIdx(): int;

    public function getAttacker(): CreatureInterface;

    public function getDefender(): CreatureInterface;

    public function getNextAttacker(): ?CreatureInterface;

    public function setNextAttacker(?CreatureInterface $nextAttacker): void;

    public function getNextDefender(): ?CreatureInterface;

    public function setNextDefender(?CreatureInterface $nextDefender): void;

    public function getDamage(): int;

    public function setDamage(int $damage): void;

    public function getDefenderOldHealth(): int;

    public function getDefenderNewHealth(): int;

    /** @return SkillInterface[] */
    public function getAttackerSkillsUsed(): array;

    public function addAttackerSkillUsed(SkillInterface $skill): void;

    /** @return SkillInterface[] */
    public function getDefenderSkillsUsed(): array;

    public function addDefenderSkillUsed(SkillInterface $skill): void;

    public function doDamage(): void;
}