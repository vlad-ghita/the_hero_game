<?php declare(strict_types=1);

namespace App\Battle;

use App\Creature\CreatureInterface;

class BattleReport implements BattleReportInterface
{
    /** @var BattleRoundInterface[] */
    private array $rounds = [];

    public function __construct(
        private CreatureInterface $creature1,
        private CreatureInterface $creature2,
    )
    {
    }

    public function getRounds(): array
    {
        return $this->rounds;
    }

    public function addRound(BattleRoundInterface $round): void
    {
        array_push($this->rounds, $round);
    }

    public function winnerExists(): bool
    {
        return $this->creature1->getHealth() <= 0 || $this->creature2->getHealth() <= 0;
    }

    public function getWinner(): CreatureInterface
    {
        return $this->creature1->getHealth() < $this->creature2->getHealth() ? $this->creature2 : $this->creature1;
    }

}