<?php declare(strict_types=1);

namespace App\Attribute;

use App\Exception\InvalidArgumentException;

/**
 * When present, indicates property can be randomized.
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::IS_REPEATABLE)]
class CanRandomize
{
    public const WHEN_NULL = 1;
    public const WHEN_ALWAYS = 2;
    public const WHEN = [self::WHEN_NULL, self::WHEN_ALWAYS];

    public function __construct(
        public int $when = self::WHEN_NULL,
    )
    {
        if (!in_array($this->when, self::WHEN)) {
            throw new InvalidArgumentException(sprintf('$when must be one of %s', implode(',', self::WHEN)));
        }
    }

}