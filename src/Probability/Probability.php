<?php declare(strict_types=1);

namespace App\Probability;

use App\Pattern\BuilderInterface;

abstract class Probability implements ProbabilityInterface
{
    /** @var ChanceBuilderInterface */
    protected BuilderInterface $chanceBuilder;

    public function __construct(
        BuilderInterface $chanceBuilder
    )
    {
        $this->chanceBuilder = $chanceBuilder;
    }

    public function fromValues(int $min, int $max, int $value): bool
    {
        $this->chanceBuilder->setMin($min);
        $this->chanceBuilder->setMax($max);
        $this->chanceBuilder->setValue($value);

        return $this->fromChance($this->chanceBuilder->build());
    }

}