<?php declare(strict_types=1);

namespace App\Probability;

use App\Pattern\BuilderInterface;

interface ChanceBuilderInterface extends BuilderInterface, ChanceInterface
{
    public function build(): ChanceInterface;
}