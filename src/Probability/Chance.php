<?php declare(strict_types=1);

namespace App\Probability;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Stores information about chance for an event.
 *
 * value == min means never occurring event.
 * value == max means always occurring event.
 */
class Chance implements ChanceInterface
{
    #[Assert\LessThanOrEqual(propertyPath: "max")]
    private int $min;

    #[Assert\GreaterThanOrEqual(propertyPath: "min")]
    private int $max;

    #[Assert\Range(minPropertyPath: "min", maxPropertyPath: "max")]
    private int $value;

    public function __construct(
        int $min = 0,
        int $max = 100,
        int $value = 0,
    )
    {
        $this->max = $max;
        $this->min = $min;
        $this->value = $value;
    }

    public function getMin(): int
    {
        return $this->min;
    }

    public function setMin(int $min): void
    {
        $this->min = $min;
    }

    public function getMax(): int
    {
        return $this->max;
    }

    public function setMax(int $max): void
    {
        $this->max = $max;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): void
    {
        $this->value = $value;
    }

}