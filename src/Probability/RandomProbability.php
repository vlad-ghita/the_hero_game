<?php declare(strict_types=1);

namespace App\Probability;

/**
 * Generates uncorrelated random events.
 */
class RandomProbability extends Probability
{

    public function fromChance(ChanceInterface $chance): bool
    {
        $threshold = mt_rand($chance->getMin(), $chance->getMax());

        return $chance->getValue() === $chance->getMax() || $threshold < $chance->getValue();
    }

}