<?php declare(strict_types=1);

namespace App\Probability;


/**
 * Stores information about chance for an event.
 *
 * value == min means never occurring event.
 * value == max means always occurring event.
 */
interface ChanceInterface
{
    public function getMin(): int;

    public function setMin(int $min): void;

    public function getMax(): int;

    public function setMax(int $max): void;

    public function getValue(): int;

    public function setValue(int $value): void;
}