<?php declare(strict_types=1);

namespace App\Probability;

/**
 * Generates events given a chance.
 */
interface ProbabilityInterface
{
    /**
     * Given a chance, generates a random event.
     * Returns true if event matched the chance, false otherwise.
     *
     * Eg: given 30% chance of having luck, then (true would mean lucky) and (false would mean unlucky).
     *
     * @param ChanceInterface $chance
     * @return bool
     */
    public function fromChance(ChanceInterface $chance): bool;

    public function fromValues(int $min, int $max, int $value): bool;

}