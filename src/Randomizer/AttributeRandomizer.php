<?php declare(strict_types=1);

namespace App\Randomizer;

use App\Attribute\CanRandomize;
use ReflectionClass;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Randomizes using attributes.
 *
 * Supports only @see CanRandomize attribute.
 * For each property, randomizes based on @see Range validator.
 */
class AttributeRandomizer implements RandomizerInterface
{

    public function __construct(
        private ?PropertyAccessorInterface $propertyAccessor = null,
    )
    {
    }

    public function randomize(object $object): void
    {
        $reflectionClass = new ReflectionClass($object);

        foreach ($reflectionClass->getProperties() as $property) {
            /** @var CanRandomize $canRandomize */
            if (null === $canRandomize = $this->getAttribute($property, CanRandomize::class)) {
                continue;
            }

            $value = $this->getPropertyAccessor()->getValue($object, $property->getName());

            if (!(
                (null === $value && $canRandomize->when === CanRandomize::WHEN_NULL)
                || $canRandomize->when === CanRandomize::WHEN_ALWAYS
            )
            ) {
                continue;
            }

            /** @var Range $range */
            if (null === $range = $this->getAttribute($property, Range::class)) {
                continue;
            }

            $min = $this->getLimit($object, $range->minPropertyPath, $range->min);
            $max = $this->getLimit($object, $range->maxPropertyPath, $range->max);

            if (!is_int($min) || !is_int($max)) {
                continue;
            }

            $value = mt_rand($min, $max);

            $this->getPropertyAccessor()->setValue($object, $property->getName(), $value);
        }
    }

    private function getPropertyAccessor(): PropertyAccessorInterface
    {
        if (null === $this->propertyAccessor) {
            $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        }

        return $this->propertyAccessor;
    }

    private function getLimit(?object $object, ?string $propertyPath, ?int $limit): mixed
    {
        if (null !== $propertyPath) {
            return $this->getPropertyAccessor()->getValue($object, $propertyPath);
        }

        return $limit;
    }

    private function getAttribute(\ReflectionProperty $property, string $name): ?object
    {
        if (0 === count($attributes = $property->getAttributes($name))) {
            return null;
        }

        return $attributes[0]->newInstance();
    }

}