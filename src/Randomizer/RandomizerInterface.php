<?php declare(strict_types=1);

namespace App\Randomizer;

interface RandomizerInterface
{

    /**
     * Randomizes given object.
     *
     * @param object $object
     */
    public function randomize(object $object): void;

}