<?php declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{

    public function __construct(
        private ConstraintViolationListInterface $violations
    )
    {
        parent::__construct(implode(PHP_EOL, array_merge(['Validation failed.'], $this->getJoinedMessages())));
    }

    public function getMessages(): array
    {
        $messages = [];

        /** @var ConstraintViolation $violation */
        foreach ($this->violations as $violation) {
            $messages[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $messages;
    }

    public function getJoinedMessages(): array
    {
        $messages = [];

        foreach ($this->getMessages() as $key => $errors) {
            $messages[$key] = $key . ': ' . implode(' ', $errors);
        }

        return $messages;
    }

}