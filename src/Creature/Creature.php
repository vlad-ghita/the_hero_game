<?php declare(strict_types=1);

namespace App\Creature;

use App\Attribute\CanRandomize;
use App\Skill\PercentSkillInterface;
use App\Skill\SkillInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Creature implements CreatureInterface
{

    #[Assert\NotBlank]
    private ?string $name;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(value: 0)]
    #[Assert\LessThanOrEqual(propertyPath: "maxHealth")]
    private ?int $minHealth;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(propertyPath: "minHealth")]
    private ?int $maxHealth;

    #[Assert\NotBlank]
    #[Assert\Range(minPropertyPath: "minHealth", maxPropertyPath: "maxHealth")]
    #[CanRandomize]
    private ?int $health;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(value: 0)]
    #[Assert\LessThanOrEqual(propertyPath: "maxStrength")]
    private ?int $minStrength;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(propertyPath: "minStrength")]
    private ?int $maxStrength;

    #[Assert\NotBlank]
    #[Assert\Range(minPropertyPath: "minStrength", maxPropertyPath: "maxStrength")]
    #[CanRandomize]
    private ?int $strength;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(value: 0)]
    #[Assert\LessThanOrEqual(propertyPath: "maxDefence")]
    private ?int $minDefence;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(propertyPath: "minDefence")]
    private ?int $maxDefence;

    #[Assert\NotBlank]
    #[Assert\Range(minPropertyPath: "minDefence", maxPropertyPath: "maxDefence")]
    #[CanRandomize]
    private ?int $defence;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(value: 0)]
    #[Assert\LessThanOrEqual(propertyPath: "maxSpeed")]
    private ?int $minSpeed;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(propertyPath: "minSpeed")]
    private ?int $maxSpeed;

    #[Assert\NotBlank]
    #[Assert\Range(minPropertyPath: "minSpeed", maxPropertyPath: "maxSpeed")]
    #[CanRandomize]
    private ?int $speed;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(value: PercentSkillInterface::MIN)]
    #[Assert\LessThanOrEqual(propertyPath: "maxLuck")]
    private ?int $minLuck;

    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(propertyPath: "minLuck")]
    #[Assert\LessThanOrEqual(value: PercentSkillInterface::MAX)]
    private ?int $maxLuck;

    #[Assert\NotBlank]
    #[Assert\Range(minPropertyPath: "minLuck", maxPropertyPath: "maxLuck")]
    #[CanRandomize]
    private ?int $luck;

    /** @var SkillInterface[] */
    private array $skills = [];

    public function __construct(
        ?string $name = null,
        ?int    $minHealth = null,
        ?int    $maxHealth = null,
        ?int    $health = null,
        ?int    $minStrength = null,
        ?int    $maxStrength = null,
        ?int    $strength = null,
        ?int    $minDefence = null,
        ?int    $maxDefence = null,
        ?int    $defence = null,
        ?int    $minSpeed = null,
        ?int    $maxSpeed = null,
        ?int    $speed = null,
        ?int    $minLuck = null,
        ?int    $maxLuck = null,
        ?int    $luck = null,
        ?array  $skills = [],
    )
    {
        $this->name = $name;
        $this->minHealth = $minHealth;
        $this->maxHealth = $maxHealth;
        $this->health = $health;
        $this->minStrength = $minStrength;
        $this->maxStrength = $maxStrength;
        $this->strength = $strength;
        $this->minDefence = $minDefence;
        $this->maxDefence = $maxDefence;
        $this->defence = $defence;
        $this->minSpeed = $minSpeed;
        $this->maxSpeed = $maxSpeed;
        $this->speed = $speed;
        $this->minLuck = $minLuck;
        $this->maxLuck = $maxLuck;
        $this->luck = $luck;
        $this->addSkills($skills);
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getMinHealth(): ?int
    {
        return $this->minHealth;
    }

    public function setMinHealth(?int $minHealth): void
    {
        $this->minHealth = $minHealth;
    }

    public function getMaxHealth(): ?int
    {
        return $this->maxHealth;
    }

    public function setMaxHealth(?int $maxHealth): void
    {
        $this->maxHealth = $maxHealth;
    }

    public function getHealth(): ?int
    {
        return $this->health;
    }

    public function setHealth(?int $health): void
    {
        $this->health = $health;
    }

    public function getMinStrength(): ?int
    {
        return $this->minStrength;
    }

    public function setMinStrength(?int $minStrength): void
    {
        $this->minStrength = $minStrength;
    }

    public function getMaxStrength(): ?int
    {
        return $this->maxStrength;
    }

    public function setMaxStrength(?int $maxStrength): void
    {
        $this->maxStrength = $maxStrength;
    }

    public function getStrength(): ?int
    {
        return $this->strength;
    }

    public function setStrength(?int $strength): void
    {
        $this->strength = $strength;
    }

    public function getMinDefence(): ?int
    {
        return $this->minDefence;
    }

    public function setMinDefence(?int $minDefence): void
    {
        $this->minDefence = $minDefence;
    }

    public function getMaxDefence(): ?int
    {
        return $this->maxDefence;
    }

    public function setMaxDefence(?int $maxDefence): void
    {
        $this->maxDefence = $maxDefence;
    }

    public function getDefence(): ?int
    {
        return $this->defence;
    }

    public function setDefence(?int $defence): void
    {
        $this->defence = $defence;
    }

    public function getMinSpeed(): ?int
    {
        return $this->minSpeed;
    }

    public function setMinSpeed(?int $minSpeed): void
    {
        $this->minSpeed = $minSpeed;
    }

    public function getMaxSpeed(): ?int
    {
        return $this->maxSpeed;
    }

    public function setMaxSpeed(?int $maxSpeed): void
    {
        $this->maxSpeed = $maxSpeed;
    }

    public function getSpeed(): ?int
    {
        return $this->speed;
    }

    public function setSpeed(?int $speed): void
    {
        $this->speed = $speed;
    }

    public function getMinLuck(): ?int
    {
        return $this->minLuck;
    }

    public function setMinLuck(?int $minLuck): void
    {
        $this->minLuck = $minLuck;
    }

    public function getMaxLuck(): ?int
    {
        return $this->maxLuck;
    }

    public function setMaxLuck(?int $maxLuck): void
    {
        $this->maxLuck = $maxLuck;
    }

    public function getLuck(): ?int
    {
        return $this->luck;
    }

    public function setLuck(?int $luck): void
    {
        $this->luck = $luck;
    }

    public function getSkills(): array
    {
        return $this->skills;
    }

    public function addSkill(SkillInterface $skill): void
    {
        $skill->setOwner($this);
        $this->skills[] = $skill;
    }

    public function addSkills(array $skills): void
    {
        array_walk($skills, fn($skill) => $this->addSkill($skill));
    }

    public function clearSkills(): void
    {
        $this->skills = [];
    }

    public function takeDamage(int $damage): void
    {
        $this->health -= $damage;
    }

    public function __toString(): string
    {
        return $this->getName();
    }

}