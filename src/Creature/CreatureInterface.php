<?php declare(strict_types=1);

namespace App\Creature;

use App\Skill\SkillInterface;

interface CreatureInterface
{

    public function getName(): ?string;

    public function setName(?string $name): void;

    public function getMinHealth(): ?int;

    public function setMinHealth(?int $minHealth): void;

    public function getMaxHealth(): ?int;

    public function setMaxHealth(?int $maxHealth): void;

    public function getHealth(): ?int;

    public function setHealth(?int $health): void;

    public function getMinStrength(): ?int;

    public function setMinStrength(?int $minStrength): void;

    public function getMaxStrength(): ?int;

    public function setMaxStrength(?int $maxStrength): void;

    public function getStrength(): ?int;

    public function setStrength(?int $strength): void;

    public function getMinDefence(): ?int;

    public function setMinDefence(?int $minDefence): void;

    public function getMaxDefence(): ?int;

    public function setMaxDefence(?int $maxDefence): void;

    public function getDefence(): ?int;

    public function setDefence(?int $defence): void;

    public function getMinSpeed(): ?int;

    public function setMinSpeed(?int $minSpeed): void;

    public function getMaxSpeed(): ?int;

    public function setMaxSpeed(?int $maxSpeed): void;

    public function getSpeed(): ?int;

    public function setSpeed(?int $speed): void;

    public function getMinLuck(): ?int;

    public function setMinLuck(?int $minLuck): void;

    public function getMaxLuck(): ?int;

    public function setMaxLuck(?int $maxLuck): void;

    public function getLuck(): ?int;

    public function setLuck(?int $luck): void;

    /** @return SkillInterface[] */
    public function getSkills(): array;

    public function addSkill(SkillInterface $skill): void;

    /** @param SkillInterface[] $skills */
    public function addSkills(array $skills): void;

    public function clearSkills(): void;

    public function takeDamage(int $damage): void;

}