<?php declare(strict_types=1);

namespace App\Creature;

use App\Pattern\BuilderInterface;

interface CreatureBuilderInterface extends BuilderInterface, CreatureInterface
{
    public function build(): CreatureInterface;
}