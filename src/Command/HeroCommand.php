<?php declare(strict_types=1);

namespace App\Command;

use App\Battle\BattleInterface;
use App\Creature\CreatureBuilderInterface;
use App\Creature\CreatureInterface;
use App\Exception\ValidationException;
use App\Skill\LuckSkill;
use App\Skill\MagicShieldSkill;
use App\Skill\RapidStrikeSkill;
use App\Pattern\BuilderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HeroCommand extends Command
{
    const ORDERUS = 'Orderus';
    const WILD_BEAST = 'Wild Beast';

    protected static $defaultName = 'hero';

    /** @var CreatureBuilderInterface $orderusBuilder */
    private BuilderInterface $orderusBuilder;

    /** @var CreatureBuilderInterface $beastBuilder */
    private BuilderInterface $beastBuilder;

    public function __construct(
        BuilderInterface              $orderusBuilder,
        BuilderInterface              $beastBuilder,
        private BattleInterface       $battle,
        private ParameterBagInterface $params,
    )
    {
        $this->beastBuilder = $beastBuilder;
        $this->orderusBuilder = $orderusBuilder;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(sprintf('Runs a battle between random "%s" & "%s".', self::ORDERUS, self::WILD_BEAST));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $io->title(self::ORDERUS . ' vs ' . self::WILD_BEAST);

            $io->writeln('Setup ' . self::ORDERUS . '.');
            $this->orderusBuilder->setName(self::ORDERUS);
            $this->orderusBuilder->addSkills([
                new LuckSkill(),
                new MagicShieldSkill($this->params->get('app.orderus.magic_shield_chance')),
                new RapidStrikeSkill($this->params->get('app.orderus.rapid_strike_chance')),
            ]);
            $orderus = $this->orderusBuilder->build();

            $io->writeln('Setup ' . self::WILD_BEAST . '.');
            $this->beastBuilder->setName(self::WILD_BEAST);
            $this->beastBuilder->addSkills([
                new LuckSkill(),
            ]);
            $beast = $this->beastBuilder->build();

            $io->table(
                ['Name', 'Health', 'Strength', 'Defence', 'Speed', 'Luck'],
                array_map(function (CreatureInterface $creature) {
                    return [
                        $creature->getName(),
                        $creature->getHealth(),
                        $creature->getStrength(),
                        $creature->getDefence(),
                        $creature->getSpeed(),
                        $creature->getLuck(),
                    ];
                }, [$orderus, $beast])
            );

            $report = $this->battle->fight($orderus, $beast);

            $io->writeln('Battle report:');
            $io->table(
                ['Round #', 'Attacker', 'Defender', 'Damage', 'Health change', 'Attacker skills used', 'Defender skills used'],
                array_map(function ($round) {
                    return [
                        $round->getIdx(),
                        $round->getAttacker()->getName(),
                        $round->getDefender()->getName(),
                        $round->getDamage(),
                        $round->getDefenderOldHealth() . ' -> ' . $round->getDefenderNewHealth(),
                        implode(', ', $round->getAttackerSkillsUsed()),
                        implode(', ', $round->getDefenderSkillsUsed()),
                    ];
                }, $report->getRounds())
            );

            if ($report->winnerExists()) {
                $io->success(sprintf('After %d rounds, winner is "%s".', count($report->getRounds()), $report->getWinner()->getName()));
            } else {
                $io->warning(sprintf('No winner determined in %d rounds.', count($report->getRounds())));
            }

            return Command::SUCCESS;
        } catch (ValidationException $e) {
            $io->error($e->getMessage());

            return Command::FAILURE;
        }
    }
}