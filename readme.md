# The Hero Game

## Requirements

See "The Hero Game.pdf"

## Installation

1. Setup local PHP as described in [Symfony The Fast Track](https://symfony.com/doc/current/the-fast-track/en/1-tools.html).
2. Clone repository.
3. Install composer dependencies: `symfony composer install`

## Usage 

1. On Windows, run script: `.\hero.bat` 
2. On Linux, run command: `php bin/console hero`
