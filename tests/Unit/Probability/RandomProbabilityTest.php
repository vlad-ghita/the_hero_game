<?php declare(strict_types=1);

namespace App\Tests\Unit\Probability;

use App\Pattern\ObjectBuilder;
use App\Probability\Chance;
use App\Probability\ChanceFactory;
use App\Probability\RandomProbability;
use PHPUnit\Framework\TestCase;

class RandomProbabilityTest extends TestCase
{

    /**
     * @dataProvider rollDataProvider
     */
    public function testRoll($chance, $exp)
    {
        $chanceBuilder = new ObjectBuilder(Chance::class);
        $probability = new RandomProbability($chanceBuilder);
        $act = $probability->fromChance($chance);
        $this->assertEquals($exp, $act);
    }

    public function rollDataProvider(): array
    {
        return [
            [new Chance(0, 100, 0), false],
            [new Chance(0, 100, 100), true],
        ];
    }

}
