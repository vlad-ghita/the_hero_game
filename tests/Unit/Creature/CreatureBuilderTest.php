<?php declare(strict_types=1);

namespace App\Tests\Unit\Creature;

use App\Creature\Creature;
use App\Creature\CreatureBuilderInterface;
use App\Pattern\ObjectBuilder;
use PHPUnit\Framework\TestCase;

class CreatureBuilderTest extends TestCase
{

    public function testBuild()
    {
        /** @var CreatureBuilderInterface $creatureBuilder */
        $creatureBuilder = new ObjectBuilder(Creature::class);

        $creatureBuilder->setName("foo");
        $creatureBuilder->setHealth(51);
        $creatureBuilder->setStrength(52);
        $creatureBuilder->setDefence(53);
        $creatureBuilder->setSpeed(54);
        $creatureBuilder->setLuck(55);

        $creature = $creatureBuilder->build();

        $this->assertEquals("foo", $creature->getName());
        $this->assertEquals(51, $creature->getHealth());
        $this->assertEquals(52, $creature->getStrength());
        $this->assertEquals(53, $creature->getDefence());
        $this->assertEquals(54, $creature->getSpeed());
        $this->assertEquals(55, $creature->getLuck());
    }

}
