<?php declare(strict_types=1);

namespace App\Tests\Unit\Randomizer;

use App\Attribute\CanRandomize;
use App\Randomizer\AttributeRandomizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints as Assert;

class AttributeRandomizerTest extends TestCase
{
    public ?int $min = 10;
    public ?int $max = 20;

    #[Assert\Range(minPropertyPath: 'min', maxPropertyPath: 'max')]
    #[CanRandomize(when: CanRandomize::WHEN_NULL)]
    public ?int $value = null;

    public function testRandomize()
    {
        $randomizer = new AttributeRandomizer();
        $randomizer->randomize($this);

        $this->assertThat($this->value, $this->logicalAnd($this->greaterThanOrEqual($this->min), $this->lessThanOrEqual($this->max)));
    }


}
