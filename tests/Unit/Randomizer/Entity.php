<?php declare(strict_types=1);

namespace App\Tests\Unit\Randomizer;

use App\Attribute\CanRandomize;
use Symfony\Component\Validator\Constraints as Assert;

class Entity
{

    public ?int $min = null;
    public ?int $max = null;

    #[Assert\Range()]
    #[CanRandomize()]
    public ?int $value = null;

}