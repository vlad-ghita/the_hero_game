<?php declare(strict_types=1);

namespace App\Tests\Unit\Skill;

use App\Battle\BattleRound;
use App\Creature\Creature;
use App\Skill\MagicShieldSkill;
use App\Skill\MagicShieldSkillHandler;
use App\Probability\ProbabilityInterface;
use PHPUnit\Framework\TestCase;

class MagicShieldSkillHandlerTest extends TestCase
{

    /**
     * @dataProvider applyDataProvider
     */
    public function testApply($attackerStrength, $defenderDefence, $isActive, $expDamage)
    {
        $attacker = new Creature(strength: $attackerStrength);
        $defender = new Creature(defence: $defenderDefence);
        $round = new BattleRound(0, $attacker, $defender);
        $skill = new MagicShieldSkill(chance: 100, owner: $defender);

        $probabilityMock = $this->createMock(ProbabilityInterface::class);
        $probabilityMock->expects($this->once())->method('fromValues')->willReturn($isActive);
        $luckSkillHandler = new MagicShieldSkillHandler($probabilityMock);
        $luckSkillHandler->apply($skill, $round);

        $this->assertEquals($expDamage, $round->getDamage());
    }

    public function applyDataProvider(): array
    {
        return [
            [50, 40, true, 5],
            [50, 45, true, 3],
            [50, 40, false, 10],
        ];
    }

}
