<?php declare(strict_types=1);

namespace App\Tests\Unit\Skill;

use App\Battle\BattleRound;
use App\Creature\Creature;
use App\Skill\RapidStrikeSkill;
use App\Skill\RapidStrikeSkillHandler;
use App\Probability\ProbabilityInterface;
use PHPUnit\Framework\TestCase;

class RapidStrikeSkillHandlerTest extends TestCase
{

    /**
     * @dataProvider applyDataProvider
     */
    public function testApply($attacker, $defender, $isActive, $expAttacker, $expDefender)
    {
        $round = new BattleRound(0, $attacker, $defender);
        $skill = new RapidStrikeSkill(chance: 100, owner: $attacker);

        $probabilityMock = $this->createMock(ProbabilityInterface::class);
        $probabilityMock->expects($this->once())->method('fromValues')->willReturn($isActive);
        $luckSkillHandler = new RapidStrikeSkillHandler($probabilityMock);
        $luckSkillHandler->apply($skill, $round);

        $this->assertEquals($expAttacker, $round->getNextAttacker());
        $this->assertEquals($expDefender, $round->getNextDefender());
    }

    public function applyDataProvider(): array
    {
        $attacker = new Creature();
        $defender = new Creature();

        return [
            [$attacker, $defender, true, $attacker, $defender],
            [$attacker, $defender, false, null, null],
        ];
    }

}
