<?php declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Creature\Creature;
use PHPUnit\Framework\TestCase;

class CreatureTest extends TestCase
{

    /**
     * @dataProvider takeDamageDataProvider
     */
    public function testTakeDamage(int $initHealth, int $damage, int $expHealth)
    {
        $creature = new Creature("", $initHealth, 100, 100, 100, 100);
        $creature->takeDamage($damage);
        $this->assertEquals($expHealth, $creature->getHealth());
    }

    public function takeDamageDataProvider(): array
    {
        return [
            [100, 30, 70],
            [100, 130, -30],
        ];
    }

}
