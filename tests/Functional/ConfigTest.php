<?php declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ConfigTest extends KernelTestCase
{

    public function creatureConfigDataProvider(): array
    {
        return [
            ['orderus', 70, 100, 70, 80, 45, 55, 40, 50, 10, 30],
            ['beast', 60, 90, 60, 90, 40, 60, 40, 60, 25, 40],
        ];
    }


    /**
     * @dataProvider creatureConfigDataProvider
     */
    public function testCreatureConfig(
        $creature,
        $minHealth,
        $maxHealth,
        $minStrength,
        $maxStrength,
        $minDefence,
        $maxDefence,
        $minSpeed,
        $maxSpeed,
        $minLuck,
        $maxLuck,
    )
    {
        $kernel = self::bootKernel();
        $container = $kernel->getContainer();

        $this->assertEquals($minHealth, $container->getParameter("app.$creature.min_health"));
        $this->assertEquals($maxHealth, $container->getParameter("app.$creature.max_health"));
        $this->assertEquals($minStrength, $container->getParameter("app.$creature.min_strength"));
        $this->assertEquals($maxStrength, $container->getParameter("app.$creature.max_strength"));
        $this->assertEquals($minDefence, $container->getParameter("app.$creature.min_defence"));
        $this->assertEquals($maxDefence, $container->getParameter("app.$creature.max_defence"));
        $this->assertEquals($minSpeed, $container->getParameter("app.$creature.min_speed"));
        $this->assertEquals($maxSpeed, $container->getParameter("app.$creature.max_speed"));
        $this->assertEquals($minLuck, $container->getParameter("app.$creature.min_luck"));
        $this->assertEquals($maxLuck, $container->getParameter("app.$creature.max_luck"));
    }

}
