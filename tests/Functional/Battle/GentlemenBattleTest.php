<?php declare(strict_types=1);

namespace App\Tests\Functional\Battle;

use App\Creature\Creature;
use App\Skill\LuckSkill;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GentlemenBattleTest extends KernelTestCase
{

    public function testOrderusFlawlessVictory()
    {
        $container = self::bootKernel()->getContainer();

        $orderus = new Creature('Orderus', health: 75, strength: 73, defence: 55, speed: 45, luck: 100, skills: [new LuckSkill()]);
        $beast = new Creature('Wild beast', health: 60, strength: 85, defence: 40, speed: 40, luck: 35);
        $orderusHealth = $orderus->getHealth();

        $battle = $container->get('App\Battle\GentlemenBattle');
        $battleReport = $battle->fight($orderus, $beast);

        $this->assertEquals($orderus, $battleReport->getWinner());
        $this->assertEquals($orderus, $battleReport->getRounds()[0]->getAttacker(), 'Orderus must attack first.');
        $this->assertCount(3, $battleReport->getRounds());
        $this->assertEquals(-6, $beast->getHealth());
        $this->assertEquals($orderusHealth, $orderus->getHealth(), 'Orderus must not take damage.');
    }

}
