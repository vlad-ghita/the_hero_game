<?php declare(strict_types=1);

namespace App\Tests\Functional\Creature;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RandomCreatureBuilderTest extends KernelTestCase
{

    public function testBasicValidation()
    {
        self::bootKernel();

        $randomCreatureBuilder = self::$kernel->getContainer()->get('app.random_orderus_builder');
        $randomCreatureBuilder->setMinHealth(10);
        $randomCreatureBuilder->setMaxHealth(20);
        $randomCreatureBuilder->setMinStrength(20);
        $randomCreatureBuilder->setMaxStrength(30);
        $randomCreatureBuilder->setMinDefence(30);
        $randomCreatureBuilder->setMaxDefence(40);
        $randomCreatureBuilder->setMinSpeed(40);
        $randomCreatureBuilder->setMaxSpeed(50);
        $randomCreatureBuilder->setMinLuck(50);
        $randomCreatureBuilder->setMaxLuck(60);
        $creature = $randomCreatureBuilder->build();

        $this->assertThat($creature->getHealth(), $this->logicalAnd($this->greaterThanOrEqual(10), $this->lessThanOrEqual(20)));
        $this->assertThat($creature->getStrength(), $this->logicalAnd($this->greaterThanOrEqual(20), $this->lessThanOrEqual(30)));
        $this->assertThat($creature->getDefence(), $this->logicalAnd($this->greaterThanOrEqual(30), $this->lessThanOrEqual(40)));
        $this->assertThat($creature->getSpeed(), $this->logicalAnd($this->greaterThanOrEqual(40), $this->lessThanOrEqual(50)));
        $this->assertThat($creature->getLuck(), $this->logicalAnd($this->greaterThanOrEqual(50), $this->lessThanOrEqual(60)));
    }

}
