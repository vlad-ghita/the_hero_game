<?php declare(strict_types=1);

namespace App\Tests\Functional\Creature;

use App\Creature\Creature;
use App\Exception\ValidationException;
use App\Pattern\BuilderDecoratorInterface;
use App\Pattern\ValidationBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ValidationCreatureBuilderTest extends KernelTestCase
{

    /**
     * @dataProvider basicValidationDataProvider
     */
    public function testBasicValidation($property, $message, $name, $health, $strength, $defence, $speed, $luck)
    {
        self::bootKernel();

        $decoratedMock = $this->createMock(BuilderDecoratorInterface::class);
        $decoratedMock->expects($this->once())->method('build')->willReturn(
            new Creature($name, $health, $strength, $defence, $speed, $luck)
        );

        // TODO get rid of validator here. Seems it's deprecated.
        $validationBuilder = new ValidationBuilder(
            $decoratedMock,
            self::$kernel->getContainer()->get('validator')
        );

        try {
            $validationBuilder->build();
        } catch (ValidationException $e) {
            $this->assertContains($message, $e->getMessages()[$property]);
            return;
        }

        $this->fail('Can\'t reach this.');
    }

    public function basicValidationDataProvider(): array
    {
        return [
            ['name', 'This value should not be blank.', null, null, null, null, null, null],
            ['minHealth', 'This value should not be blank.', null, null, null, null, null, null],
            ['maxHealth', 'This value should not be blank.', null, null, null, null, null, null],
            ['health', 'This value should not be blank.', null, null, null, null, null, null],
            ['minStrength', 'This value should not be blank.', null, null, null, null, null, null],
            ['maxStrength', 'This value should not be blank.', null, null, null, null, null, null],
            ['strength', 'This value should not be blank.', null, null, null, null, null, null],
            ['minDefence', 'This value should not be blank.', null, null, null, null, null, null],
            ['maxDefence', 'This value should not be blank.', null, null, null, null, null, null],
            ['defence', 'This value should not be blank.', null, null, null, null, null, null],
            ['minSpeed', 'This value should not be blank.', null, null, null, null, null, null],
            ['maxSpeed', 'This value should not be blank.', null, null, null, null, null, null],
            ['speed', 'This value should not be blank.', null, null, null, null, null, null],
            ['minLuck', 'This value should not be blank.', null, null, null, null, null, null],
            ['maxLuck', 'This value should not be blank.', null, null, null, null, null, null],
            ['luck', 'This value should not be blank.', null, null, null, null, null, null],
        ];
    }

}
